# snmptool #

快速获取交换机和路由器的常用信息。

本包是对 `"github.com/soniah/gosnmp"` 的一个封装。

### 安装 ###

```
go get "github.com/soniah/gosnmp"
go get "bitbucket.org/followgo/snmptool"
```

### 使用 ###

请参考单元测试。

### 功能列表 ###

[x] 获取系统信息
[x] 设置系统信息
[x] 获取端口列表
[ ] 获取 802.1Q VLAN 和端口的对应表
[ ] 通过 LLDP 协议获取设备的连线
[ ] 获取设备 STP 信息
[ ] 创建 VLAN 和配置端口
